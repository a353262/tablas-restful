# Práctica | Tarea Tablas Restful

## Getting Started

El contenido de este repositorio es la creación de una API REST basandonos en la siguiente tabla restful:

* GET /results/:n1/:n2 -> Sumar n1 + n2

* POST /results/ -> Multiplicar n1 * n2

* PUT /results/ -> Dividir n1 / n2

* PATCH /results/ -> Potencia n1 ^ n2

* DELETE /results/:n1/:n2 -> restar n1 - n2

Estas rutas y métodos HTTP permiten realizar distintas operaciones matemáticas de dos números proporcionados, 
dependiendo de la ruta y el método utilizados en la solicitud. Cada ruta realiza una operación específica y 
devuelve el resultado que corresponda.

### Prerequisitos

    *node js 
    *npm 
    *postman

### Installing

1.Clona este respositorio 

    git clone https://gitlab.com/a353262/tablas-restful.git

2.Posicionate en la carpeta del proyecto 

    cd tablasrestful

3.Ejecuta el siguiente comando,esto hará que este corriendo el servidor: 

    node index.js


4.Abre Postman para realizar las solicitudes HTTP  a las rutas y metodos de la API

 
* En la barra donde se realiza el request ingresa la siguiente url :

    http://localhost:4000/results/n1/n2 (sustituyendo n1 y n2 por numeros deseados)

* Para probar cada método,en la parte izquierda de la barra da click y se desplegarán los distintos métodos a probar (GET,POST,PUT,DELETE,PATCH),selecciona el que desees dentro de los que se incluyeron en la tabla restful

* Realiza un send dando click en el botón "send",esto envía la solicitud HTTP al servidor 

* Se desplegará la respuesta del servidor en la parte inferior de la ventana de Postman


## Built With

* node js
* npm

## Authors
Anahí Peinado Villalobos 353262

## License

No cuenta con licencia

## Acknowledgments
* I.S Luis Antonio Ramírez Martínez 
