const express = require('express');

const app = express(); // inciializamos la libreria de express



//implementación de la tabla restul 

/*
GET /results/:n1/:n2 -> Sumar n1 + n2

POST /results/ -> Multiplicar n1 * n2

PUT /results/ -> Dividir n1 / n2

PATCH /results/ -> Potencia n1 ^ n2


*/ 

app.get('/results/:n1/:n2',(request,response)=>{

    const n1 = parseInt(request.params.n1);

    const n2 = parseInt(request.params.n2);

    const suma = n1 + n2;

    response.send(`La suma es : ${suma}`);
});


app.post('/results/:n1/:n2',(request,response)=>{

    const n1 = parseInt(request.params.n1);

    const n2 = parseInt(request.params.n2);

    const multiplicacion = n1 * n2;

    response.send(`La multiplicacion es : ${multiplicacion}`);
});



app.put('/results/:n1/:n2',(request,response)=>{

    const n1 = parseInt(request.params.n1);

    const n2 = parseInt(request.params.n2);

    const division = n1 / n2;

    response.send(`EL resultado de la división es : ${division}`);
});


app.patch('/results/:n1/n2',(request,response)=>{

    const n1 = parseInt(request.params.n1);

    const n2 = parseInt(request.params.n2);

    const potencia = n1 ** n2;

    response.send(`EL resultado de la potencia es : ${potencia}`);
});

app.delete('/results/:n1/:n2',(request,response)=>{

    const n1 = parseInt(request.params.n1);

    const n2 = parseInt(request.params.n2);

    const resta = n1 - n2;

    response.send(`EL resultado de la resta es : ${resta}`);
});









app.listen(4000,()=>{
    console.log("Server is running on port 4000");
});










